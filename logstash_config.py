import logging
from logstash_async.handler import AsynchronousLogstashHandler
from logstash_async.handler import LogstashFormatter

global logger

def logstash_config():
    logger = logging.getLogger("logstash")
    logger.setLevel(logging.DEBUG)        

    # Create the handler
    handler = AsynchronousLogstashHandler(
        host='5fa4ab98-91f6-47b0-be17-3712c6a95dda-ls.logit.io', 
        port=19889, 
        ssl_enable=True, 
        ssl_verify=False,
        database_path='')
    # Here you can specify additional formatting on your log record/message
    formatter = LogstashFormatter()
    handler.setFormatter(formatter)

    # Assign handler to the logger
    logger.addHandler(handler)