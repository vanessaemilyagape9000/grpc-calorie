from asyncio.log import logger
import grpc
import calorie_pb2_grpc
import calorie_pb2
from concurrent import futures
import time
from grpc_reflection.v1alpha import reflection
from config import config
import psycopg2
from logstash_config import logstash_config

class CalorieServicer(calorie_pb2_grpc.CalorieServiceServicer):
    def CalorieGet(self, request, context):
        try:
            params = config()

            # connect to the PostgreSQL server
            print('Connecting to the PostgreSQL database...')
            conn = psycopg2.connect(**params)

            # create a cursor
            cur = conn.cursor()
            cur.execute("select * from calorie where id=%s;" % (request.userID))
            calData = cur.fetchone()
            if calData is None:
                cur.execute("insert into calorie (id, totalcal, foodcal, workcal) values (%s, 0, 0, 0);" % (request.userID))
                conn.commit()
                cur.execute("select * from calorie where id=%s;" % (request.userID))
                calData = cur.fetchone()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            response = calorie_pb2.GetResponse()
            response.totalCal = calData[1]
            response.foodCal = calData[2]
            response.workCal = calData[3]
            logger.debug("Accessed get calorie service")
            if conn:
                cur.close()
                conn.close()
                print("PostgreSQL connection is closed")
            return response

    def CalorieAdd(self, request, context):
        try:
            params = config()

            # connect to the PostgreSQL server
            print('Connecting to the PostgreSQL database...')
            conn = psycopg2.connect(**params)
            
            # create a cursor
            cur = conn.cursor()
            cur.execute("select * from calorie where id=%s;" % (request.userID))
            calData = cur.fetchone()
            if calData is None:
                cur.execute("insert into calorie (id, totalcal, foodcal, workcal) values (%s, 0, 0, 0);" % (request.userID))
                conn.commit()
                cur.execute("select * from calorie where id=%s;" % (request.userID))
                calData = cur.fetchone()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            response = calorie_pb2.AddResponse()
            response.addResult = request.calorie + calData[1]
            response.foodCal = request.calorie + calData[2]
            response.workCal = calData[3]
            cur.execute("update calorie set totalcal=%s, foodcal=%s where id=%s;" % (response.addResult,response.foodCal,request.userID))
            conn.commit()
            logger.debug("Accessed add calorie service")
            if conn:
                cur.close()
                conn.close()
                print("PostgreSQL connection is closed")
            return response

    def CalorieSub(self, request, context):
        try:
            params = config()

            # connect to the PostgreSQL server
            print('Connecting to the PostgreSQL database...')
            conn = psycopg2.connect(**params)
            
            # create a cursor
            cur = conn.cursor()
            cur.execute("select * from calorie where id=%s;" % (request.userID))
            calData = cur.fetchone()
            if calData is None:
                cur.execute("insert into calorie (id, totalcal, foodcal, workcal) values (%s, 0, 0, 0);" % (request.userID))
                conn.commit()
                cur.execute("select * from calorie where id=%s;" % (request.userID))
                calData = cur.fetchone()
        except (Exception, psycopg2.DatabaseError) as error:
            print(error)
        finally:
            response = calorie_pb2.SubResponse()
            response.subResult = calData[1] - request.calorie
            response.foodCal = calData[2]
            response.workCal = request.calorie + calData[3]
            cur.execute("update calorie set totalcal=%s, workcal=%s where id=%s;" % (response.subResult,response.workCal,request.userID))
            conn.commit()
            logger.debug("Accessed sub calorie service")
            if conn:
                cur.close()
                conn.close()
                print("PostgreSQL connection is closed")
            return response


def main():
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=10))

    calorie_pb2_grpc.add_CalorieServiceServicer_to_server(
        CalorieServicer(), server)

    SERVICE_NAMES = (
        calorie_pb2.DESCRIPTOR.services_by_name['CalorieService'].full_name,
        reflection.SERVICE_NAME,
    )
    reflection.enable_server_reflection(SERVICE_NAMES, server)

    print('Starting server. Listening on port 50050.')
    server.add_insecure_port('[::]:50050')
    server.start()

    try:
        while True:
            time.sleep(86400)

    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    logstash_config()
    main()
