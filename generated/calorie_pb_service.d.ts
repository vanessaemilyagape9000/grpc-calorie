// package: calorie
// file: calorie.proto

import * as calorie_pb from "./calorie_pb";
import {grpc} from "@improbable-eng/grpc-web";

type CalorieServiceCalorieGet = {
  readonly methodName: string;
  readonly service: typeof CalorieService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof calorie_pb.UserRequest;
  readonly responseType: typeof calorie_pb.GetResponse;
};

type CalorieServiceCalorieAdd = {
  readonly methodName: string;
  readonly service: typeof CalorieService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof calorie_pb.CalorieRequest;
  readonly responseType: typeof calorie_pb.AddResponse;
};

type CalorieServiceCalorieSub = {
  readonly methodName: string;
  readonly service: typeof CalorieService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof calorie_pb.CalorieRequest;
  readonly responseType: typeof calorie_pb.SubResponse;
};

export class CalorieService {
  static readonly serviceName: string;
  static readonly CalorieGet: CalorieServiceCalorieGet;
  static readonly CalorieAdd: CalorieServiceCalorieAdd;
  static readonly CalorieSub: CalorieServiceCalorieSub;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class CalorieServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  calorieGet(
    requestMessage: calorie_pb.UserRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: calorie_pb.GetResponse|null) => void
  ): UnaryResponse;
  calorieGet(
    requestMessage: calorie_pb.UserRequest,
    callback: (error: ServiceError|null, responseMessage: calorie_pb.GetResponse|null) => void
  ): UnaryResponse;
  calorieAdd(
    requestMessage: calorie_pb.CalorieRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: calorie_pb.AddResponse|null) => void
  ): UnaryResponse;
  calorieAdd(
    requestMessage: calorie_pb.CalorieRequest,
    callback: (error: ServiceError|null, responseMessage: calorie_pb.AddResponse|null) => void
  ): UnaryResponse;
  calorieSub(
    requestMessage: calorie_pb.CalorieRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: calorie_pb.SubResponse|null) => void
  ): UnaryResponse;
  calorieSub(
    requestMessage: calorie_pb.CalorieRequest,
    callback: (error: ServiceError|null, responseMessage: calorie_pb.SubResponse|null) => void
  ): UnaryResponse;
}

