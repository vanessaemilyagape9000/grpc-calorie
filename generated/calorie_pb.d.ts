// package: calorie
// file: calorie.proto

import * as jspb from "google-protobuf";

export class UserRequest extends jspb.Message {
  getUserid(): number;
  setUserid(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UserRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UserRequest): UserRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UserRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UserRequest;
  static deserializeBinaryFromReader(message: UserRequest, reader: jspb.BinaryReader): UserRequest;
}

export namespace UserRequest {
  export type AsObject = {
    userid: number,
  }
}

export class CalorieRequest extends jspb.Message {
  getUserid(): number;
  setUserid(value: number): void;

  getCalorie(): number;
  setCalorie(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CalorieRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CalorieRequest): CalorieRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CalorieRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CalorieRequest;
  static deserializeBinaryFromReader(message: CalorieRequest, reader: jspb.BinaryReader): CalorieRequest;
}

export namespace CalorieRequest {
  export type AsObject = {
    userid: number,
    calorie: number,
  }
}

export class GetResponse extends jspb.Message {
  getTotalcal(): number;
  setTotalcal(value: number): void;

  getFoodcal(): number;
  setFoodcal(value: number): void;

  getWorkcal(): number;
  setWorkcal(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetResponse): GetResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetResponse;
  static deserializeBinaryFromReader(message: GetResponse, reader: jspb.BinaryReader): GetResponse;
}

export namespace GetResponse {
  export type AsObject = {
    totalcal: number,
    foodcal: number,
    workcal: number,
  }
}

export class AddResponse extends jspb.Message {
  getAddresult(): number;
  setAddresult(value: number): void;

  getFoodcal(): number;
  setFoodcal(value: number): void;

  getWorkcal(): number;
  setWorkcal(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AddResponse.AsObject;
  static toObject(includeInstance: boolean, msg: AddResponse): AddResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AddResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AddResponse;
  static deserializeBinaryFromReader(message: AddResponse, reader: jspb.BinaryReader): AddResponse;
}

export namespace AddResponse {
  export type AsObject = {
    addresult: number,
    foodcal: number,
    workcal: number,
  }
}

export class SubResponse extends jspb.Message {
  getSubresult(): number;
  setSubresult(value: number): void;

  getFoodcal(): number;
  setFoodcal(value: number): void;

  getWorkcal(): number;
  setWorkcal(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SubResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SubResponse): SubResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SubResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SubResponse;
  static deserializeBinaryFromReader(message: SubResponse, reader: jspb.BinaryReader): SubResponse;
}

export namespace SubResponse {
  export type AsObject = {
    subresult: number,
    foodcal: number,
    workcal: number,
  }
}

