// package: calorie
// file: calorie.proto

var calorie_pb = require("./calorie_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var CalorieService = (function () {
  function CalorieService() {}
  CalorieService.serviceName = "calorie.CalorieService";
  return CalorieService;
}());

CalorieService.CalorieGet = {
  methodName: "CalorieGet",
  service: CalorieService,
  requestStream: false,
  responseStream: false,
  requestType: calorie_pb.UserRequest,
  responseType: calorie_pb.GetResponse
};

CalorieService.CalorieAdd = {
  methodName: "CalorieAdd",
  service: CalorieService,
  requestStream: false,
  responseStream: false,
  requestType: calorie_pb.CalorieRequest,
  responseType: calorie_pb.AddResponse
};

CalorieService.CalorieSub = {
  methodName: "CalorieSub",
  service: CalorieService,
  requestStream: false,
  responseStream: false,
  requestType: calorie_pb.CalorieRequest,
  responseType: calorie_pb.SubResponse
};

exports.CalorieService = CalorieService;

function CalorieServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

CalorieServiceClient.prototype.calorieGet = function calorieGet(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(CalorieService.CalorieGet, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

CalorieServiceClient.prototype.calorieAdd = function calorieAdd(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(CalorieService.CalorieAdd, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

CalorieServiceClient.prototype.calorieSub = function calorieSub(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(CalorieService.CalorieSub, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.CalorieServiceClient = CalorieServiceClient;

